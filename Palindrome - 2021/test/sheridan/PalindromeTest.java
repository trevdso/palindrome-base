package sheridan;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class PalindromeTest {

	@Test
	public void testIsPalindrome( ) {
		boolean test = Palindrome.isPalindrome("racecar");
		assertTrue("Value provided failed palindrome validation...", test);
	}

	@Test
	public void testIsPalindromeNegative( ) {
		boolean test = Palindrome.isPalindrome("thisfails");
		assertFalse("Value provided failed palindrome validation...", test);
	}
	@Test
	public void testIsPalindromeBoundaryIn( ) {
		boolean test = Palindrome.isPalindrome("tacocat");
		assertTrue("Value provided failed palindrome validation...", test);
	}
	@Test
	public void testIsPalindromeBoundaryOut( ) {
		boolean test = Palindrome.isPalindrome("taco cat");
		assertTrue("Value provided failed palindrome validation...", test);
	}	
	
}
